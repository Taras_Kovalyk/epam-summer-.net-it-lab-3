using System;

namespace ConsoleApplication4
{
    class Program
    {
        class TwoDimensionalMatrix
        {
            private int Rows;
            private int Columns;
            private double[,] Array;

            TwoDimensionalMatrix()
            {

            }

            public TwoDimensionalMatrix(int Rows, int Columns)
            {
                this.Rows = Rows;
                this.Columns = Columns;
                Array = new double[Rows, Columns];
            }

            public static double KOrderMinor(int k, TwoDimensionalMatrix A)
            {
                int numRows;
                int numCols;
                int n;
                if (k < A.CountOfColumns || k < A.CountOfRows)
                {
                    numRows = k;
                    numCols = k;
                    n = numCols;
                }
                else
                {
                    throw new Exception("OutOfRangeException. You can not count this order.");
                }
              

                for (int p = 1; p < n; p++)
                {
                    for (int i=p; i< n; i++)
                    {
                        double C = A.Matrix[i, p - 1] / A.Matrix[p - 1, p - 1];
                        for(int j = 0; j < numCols; j++)
                        {
                             A.Matrix[i, j] -= C * A.Matrix[p - 1, j];
                        }
                    }
                };
                double result = 1;
                for (int i =0; i<n; i++)
                {
                    result *= A.Matrix[i, i];
                }
                return result;
            }

            public static double MinorOfElement(int RowIndex, int ColumnIndex, TwoDimensionalMatrix A)
            {
                TwoDimensionalMatrix temporaryMatrix = new TwoDimensionalMatrix();
                
                for(int i=0; i < A.CountOfRows;  i++)
                {
                    if(i==RowIndex)
                    {
                        continue;
                    }

                    for(int j=0; i < A.CountOfColumns; j++)
                    {
                        if (j == ColumnIndex)
                        {
                            continue;
                        }
                        temporaryMatrix.Matrix[i, j] = A.Matrix[i, j];
                       
                    }
                }
                TwoDimensionalMatrix.Print(temporaryMatrix);
                return 0;
            }
            public static double AddMinors(double minor1, double minor2)
            {
                return minor1 + minor2;
            }

            public static double MultiplicationMinors(double minor1, double minor2)
            {
                return minor1 * minor2;
            }

            public static double DivisionMinors(double minor1, double minor2)
            {
                return minor1 / minor2;
            }

            public int CountOfRows
            {
                get { return Rows; }
                set { Rows = value; }
            }
            
            public int CountOfColumns
            {
                get { return Columns; }
                set { Columns = value; }
            }

            public double[,] Matrix
            {
                get { return Array; }
                set { Array = value; }
            }

            public static string Print(TwoDimensionalMatrix q)
            {
                string s = "";
                for (int i = 0; i < q.CountOfRows; i++)
                {
                    for (int j = 0; j < q.CountOfColumns; j++)
                    {
                        s += q.Matrix[i, j] + "  ";
                    }
                    s += "\n";
                }
                return s;
            }
        }
        static void Main(string[] args)
        {
            TwoDimensionalMatrix w = new TwoDimensionalMatrix(4, 3);
            w.Matrix[0, 0] = 1;
            w.Matrix[0, 1] = 2;
            w.Matrix[0, 2] = 3;
            w.Matrix[1, 0] = 3;
            w.Matrix[1, 1] = 3;
            w.Matrix[1, 2] = 3;
            w.Matrix[2, 0] = 3;
            w.Matrix[2, 1] = 3;
            w.Matrix[2, 2] = 9;
            w.Matrix[3, 0] = 1;
            w.Matrix[3, 1] = 2;
            w.Matrix[3, 2] = 7;


            double minor1 = TwoDimensionalMatrix.KOrderMinor(2, w);
            double minor2 = TwoDimensionalMatrix.KOrderMinor(3, w);

            //K order minor
            try
            {
                Console.WriteLine("K order minor1 : {0}", minor1);
                Console.WriteLine("K order minor2 : {0}", minor2);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            //Additing minors
            Console.WriteLine("Additing minors : {0}", TwoDimensionalMatrix.AddMinors(minor1, minor2));

            //Multiplication minors
            Console.WriteLine("Multiplication minors : {0}", TwoDimensionalMatrix.MultiplicationMinors(minor1, minor2));

            //Division minors
            Console.WriteLine("Division minors : {0}", TwoDimensionalMatrix.DivisionMinors(minor1, minor2));

            //Minor of element
            Console.WriteLine("Minor of element : {0}", TwoDimensionalMatrix.MinorOfElement(1, 1, w));

            Console.Write(TwoDimensionalMatrix.Print(w));
            Console.Read();
        }
    }
}
